# ESP32

![](ESP32-Development-Board-Pinout.png)

## configurer arduino pour programmer un ESP

dans `Fichier > préférences` ajouter à "URL de gestionnaires de cartes supplémentaires": `https://dl.espressif.com/dl/package_esp32_index.json`

Puis dans `Outils > Type de carte > Gestionnaire de cartes`

Rechercher "ESP32" et installer les cartes

Choisir alors la bonne carte dans le choix disponible. Par exemple "NodeMCU"


### driver windows

Il peut être nécessaire d'installer un driver pour windows
https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/establish-serial-connection.html

### pins sur le node NodeMCU

https://github.com/esp8266/Arduino/blob/3e7b4b8e0cf4e1f7ad48104abfc42723b5e4f9be/variants/nodemcu/pins_arduino.h#L40

### bibliothèques utiles

ServoESP32

ESP32 analog Write


## ressources

### example MPU6050

https://randomnerdtutorials.com/esp32-mpu-6050-accelerometer-gyroscope-arduino/

### serveur web captif

https://microcontrollerslab.com/esp32-soft-access-point-web-server-in-arduino-ide/


### divers

https://www.emi.ac.ma/oumnad/ESP8266/ESP8266.html
