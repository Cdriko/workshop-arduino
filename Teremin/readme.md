# Teremin

Dans cet exemple, nous allons voir comment concevoir un programme à partir des différents exemples de capteurs et d'actionneurs.

## Le projet

On va d'abord définir ce que le système doit faire.
Ici par exemple il s'agit de la création d'un [Térémine](https://fr.wikipedia.org/wiki/Th%C3%A9r%C3%A9mine)

C'est un instrument de musique qui permet de moduler une note en bougeant la main devant un capteur de distance.  
Voici le comportement désiré.

* tout près : une note grave est produite
* plus loin : une note plus aiguë est produite
* audela d'une certaine distance (150cm), le son se coupe

Pour ce faire on va utiliser

* un capteur de distance ultrason : pour mesurer la distance de la main
* un haut parleur pour diffuser le son


## Le Pseudo code

A partir du projet, on va maintenant imaginer comment l'arduino va utiliser les dispositifs disponibles pour réaliser le comportement désiré.

Pour cela, en partant d'un croquis vide, on va écrire en commentaire les étapes de fonctionnement projetées.

Pour s'aider dans la rédaction de ce pseudo code, on consultera les exemples de chacuns des capteurs et actionneurs utilisés.

```
//appeler la bibliotheque ultrasonic

// definir la sortie associee au haut parleur



void setup(){
	//au demarrage
	
	//ouvrir une communication serie

}

void loop(){
	//mesurer la distance a l'aide du capteur ultrason
	
	//publier la mesure sur le port serie pour verifier
	
	//si la distance est inferieure a 150cm
	
		//calculer la frequence en fonction de la distance
	
		// jouer la frequence sur le haut parleur
		
	//sinon, couper le son
}
```

A ce stade, ce code ne fait rien. Il va seulement nous servir de trame pour écrire le code nécessaire.

## Implémentation

En partant de ce canevas, on va alors implémenter étape par étape l'ensemble du programme.

### capteur

Dans un premier temps on va implémenter (coder) l'entrée du capteur et publier sa mesure sur le port série, pour vérifier son bon fonctionnement à l'aide du moniteur série.

Ici , le capteur est branché sur l'entrée Digital 7.

Il a besoin d'une bibliothèque pour fonctionner. Consulter [Capteur à ultrasons](../bibiotheque/capteur/) pour voir comment l'installer.

```
//appeler la bibliotheque ultrasonic
#include "Ultrasonic.h"

Ultrasonic ultrasonic(7);//cree l'objet ultrasonic

// definir la sortie associee au haut parleur



void setup(){
	//au demarrage

	//ouvrir une communication serie
	Serial.begin(9600);
}

void loop(){
	//mesurer la distance a l'aide du capteur ultrason
	long distance=ultrasonic.MeasureInCentimeters();
	delay(200);
	//publier la mesure sur le port serie pour verifier
	Serial.println(distance);
	//si la distance est inferieure a 150cm
	
		//calculer la frequence en fonction de la distance
	
		// jouer la frequence sur le haut parleur
		
	//sinon, couper le son
}
```

En utilisant `Outils > Traceur série` on peut alors voir les différentes valeurs que prend la mesure.

Une fois validé le bon fonctionnement du capteur, on peut alors utiliser les données issues de la mesure pour actionner le haut parleur.

### actionneur

Le haut parleur est branché sur la sortie 5.

```
//appeler la bibliotheque ultrasonic
#include "Ultrasonic.h"

Ultrasonic ultrasonic(7);//cree l'objet ultrasonic

// definir la sortie associee au haut parleur
int hautParleur=5;


void setup(){
	//au demarrage
	//mettre la broche du son en sortie
  	pinMode(hautParleur,OUTPUT);
	//ouvrir une communication serie
	Serial.begin(9600);
}

void loop(){
	//mesurer la distance a l'aide du capteur ultrason
	long distance=ultrasonic.MeasureInCentimeters();
	delay(200);
	//publier la mesure sur le port serie pour verifier
	Serial.println(distance);
	//si la distance est inferieure a 150cm
	if (distance<150){	
		//calculer la frequence en fonction de la distance
		int frequence=int(map(distance,0,150,44,140));	
		// jouer la frequence sur le haut parleur
		tone(hautParleur,frequence);
		}else{
		//sinon, couper le son
		noTone(hautParleur);
		}	
}
```

Le code final est disponible dans ce dossier.