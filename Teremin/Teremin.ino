//appeler la bibliotheque ultrasonic
#include "Ultrasonic.h"

Ultrasonic ultrasonic(7);//cree l'objet ultrasonic

// definir la sortie associee au haut parleur
int hautParleur=5;


void setup(){
	//au demarrage
	//mettre la broche du son en sortie
  	pinMode(hautParleur,OUTPUT);
	//ouvrir une communication serie
	Serial.begin(9600);
}

void loop(){
	//mesurer la distance a l'aide du capteur ultrason
	long distance=ultrasonic.MeasureInCentimeters();
	delay(200);
	//publier la mesure sur le port serie pour verifier
	Serial.println(distance);
	//si la distance est inferieure a 150cm
	if (distance<150){	
		//calculer la frequence en fonction de la distance
		int frequence=int(map(distance,0,150,44,140));	
		// jouer la frequence sur le haut parleur
		tone(hautParleur,frequence);
		}else{
		//sinon, couper le son
		noTone(hautParleur);
		}	
}
