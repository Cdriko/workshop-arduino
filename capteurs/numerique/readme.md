# Entrée numérique

Arduino est capable de lire une valeur de tension absolue sur les entrées "digitales"
Elles sont numérotées de 0 à 13.

On peut qualifier ces entrées de **tout ou rien**

à noter que les deux premières (0 et 1) sont aussi utilisées pour la communication série. Il est donc déconseillé de les utiliser si on a à communiquer avec l'ordinateur.

cette entrée peut-être lue avec la commande [digitalRead](https://www.arduino.cc/reference/en/language/functions/analog-io/analogread/) et produit une valeur entre 0 (0V) et 1 (5V).

On peut y brancher n'importe quel capteur produisant une tension commutée entre 0 et 5V :

* [capteur de bascule](https://wiki.seeedstudio.com/Grove-Tilt_Switch/) : détecte un renversement
* [capteur de contact](https://wiki.seeedstudio.com/Grove-Touch_Sensor/) : détecte le contact d'un doigt
* [capteur de mouvement](https://wiki.seeedstudio.com/Grove-PIR_Motion_Sensor/) : s'active s'il perçoi un mouvement dans son champ de perception.
* [capteur infrarouge](https://wiki.seeedstudio.com/Grove-Infrared_Reflective_Sensor/) : detecte la proximité d'un obstacle
* interrupteur

