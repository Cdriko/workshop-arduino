/*
   Faire varier une frequence avec un potentiometre
   * utilisation de la fonction tone() pour générer un son
   * active le son seulement si le bouton est actionne

*/


int led = 5;
int piezzo =7;
int potentiometre = A0;
int bouton=3;// branhcer le bouton sur lentree D3


void setup() {
  // declarer la led en sortie
  pinMode(led, OUTPUT);
  //le bouton en entree
  pinMode(bouton,INPUT);
  //ouvrir une communication
  Serial.begin(9600);

}

void loop() {
  // mesurer l'entree du potentiometre
  float mesure = analogRead(potentiometre);
  // transmettre la valeur brute
  Serial.println(mesure);

  // mettre à l'echelle la valeur pour faire du son
  int frequence = int(map(mesure, 0, 1024, 50, 800));

  if(digitalRead(bouton)){//si le bouton est presse
    //fabrique le son
    tone(piezzo, frequence);
  }else{
	  //sinon eteind le son
	noTone(piezzo);
	}
}
