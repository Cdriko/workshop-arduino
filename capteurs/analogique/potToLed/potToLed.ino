/*
 * Module la lumiere d'une led avec un potentiometre
 * */


int led=5;
int potentiometre=A0;


void setup() {
  // declarer la led en sortie
  pinMode(led,OUTPUT);
  //ouvrir une communication 
  Serial.begin(9600);

}

void loop() {
  // mesurer l'entree du potentiometre
  float mesure=analogRead(potentiometre);
  // transmettre la valeur brute
  Serial.println(mesure);

  // mettre � l'echelle la valeur pour allumer la led
  int luminance=int(map(mesure,0,1024,0,255));

  //allumer la led
  analogWrite(led,luminance);  

}
