/*
   Faire varier une frequence avec un potentiometre
   * utilisation de la fonction tone() pour générer un son

*/


int led = 5;
int piezzo =7;
int potentiometre = A0;


void setup() {
  // declarer la led en sortie
  pinMode(led, OUTPUT);
  //ouvrir une communication
  Serial.begin(9600);

}

void loop() {
  // mesurer l'entree du potentiometre
  float mesure = analogRead(potentiometre);
  // transmettre la valeur brute
  Serial.println(mesure);

  // mettre à l'echelle la valeur pour faire du son
  int frequence = int(map(mesure, 0, 1024, 50, 800));

  //fabrique le son
  tone(piezzo, frequence);
}
