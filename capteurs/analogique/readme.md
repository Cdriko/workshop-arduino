# entrée analogique

Arduino est capable de lire une valeur de tension variable sur certaines entrées.
Il s'agit des entrées analogiques , numérotées de A0 à A5.

cette entrée peut-être lue avec la commande [analogRead](https://www.arduino.cc/reference/en/language/functions/analog-io/analogread/) et produit une valeur entre 0 (0V) et 1024 (5V).

On peut y brancher n'importe quel capteur produisant une tension variable :

* [potentiometre](https://wiki.seeedstudio.com/Grove-Slide_Potentiometer/)
* [capteur de niveau sonore](https://wiki.seeedstudio.com/Grove-Loudness_Sensor/#play-with-arduino)
* [capteur de luminance](https://wiki.seeedstudio.com/Grove-Luminance_Sensor/)
* [capteur d'ultraviolets](https://wiki.seeedstudio.com/Grove-UV_Sensor/)


Certains produisent deux signaux , comme par exemple le [joystick](https://wiki.seeedstudio.com/Grove-Thumb_Joystick/)