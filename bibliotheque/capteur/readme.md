# Mettre en oeuvre une bibliothèque spéciale


Certain composants ont besoin d'un code spécial pour dialoguer facilement avec l'arduino. On appelle ce code "bibliothèque" ou "library" en anglais.

Pour mettre en oeuvre ce type de capteurs, il suffit d'installer une fois pour toute la bibliothèque associée dans l'IDE arduino, pour pouvoir l'appeler par la suite.

En général, chaque bibliothèque est fournie avec quelques exemples qui permettent de comprendre comment se servir de ce composant

## exemple : le capteur à ultrason grove

![](capteurUltrason.jpeg)

[la page seeedstudio](https://wiki.seeedstudio.com/Ultra_Sonic_range_measurement_module/)

[la page de la bibliotheque](https://github.com/Seeed-Studio/Seeed_Arduino_UltrasonicRanger/blob/master/README.md)

### installation de la bibiothèque

Dans l'IDE arduino choisir le menu :
`Outils > Gérer les bibliothèques`

Dans la barre de recherche, taper "ultrasonic"

![](bibliothequeUltrasonic.jpg)

Choisir "Grove Ultrasonic Ranger" puis cliquer sur "installer"

Une fois la bibliothèque installée, un exemple est lisible dans le menu `Fichier > Exemples > Grove Ultrasonic Ranger > UltrasonicDisplayOnTerm`

### code exemple

Voici une version francisée du code exemple en question

[UltrasonicDisplayOnTerm_fr.ino](UltrasonicDisplayOnTerm_fr/UltrasonicDisplayOnTerm_fr.ino)


Il affiche la distance mesurée en face du capteur, en pouces et en centimètres.


