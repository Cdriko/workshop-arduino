/* Balayage


 by BARRAGAN <http://barraganstudio.com>
 This example code is in the public domain.
 
 modified 8 Nov 2013
 by Scott Fitzgerald
 https://arduino.cc/en/Tutorial/Sweep

Version fr C.Doutriaux
* 
* ATTENTION consulter le schema fritzing pour alimenter correctement le servomoteur !
*/
 
#include <Servo.h>//appeler la bibliotheque servo
 
Servo mon_Servo;  // Cree un objet servo pour controler le srove
// twelve servo objects can be created on most boards
 
int angle = 0;    // variable pour stocker la angle du servo
 
void setup() {
  mon_Servo.attach(5);  // attache le servo a la broche 5
}
 
void loop() {
  for (angle = 20; angle <= 170; angle += 1) { // aller de 0 a 180 degres
    // par pas de 1 degres
    mon_Servo.write(angle);              // dit au servo de bouger a la angle
    delay(15);                       // attend 15ms que le servo atteigne sa angle
  }
  for (angle = 170; angle >= 20; angle -= 1) { // aller de 180 a 0 degres
    mon_Servo.write(angle);              // dit au servo de bouger a la angle
    delay(15);                       // attend 15ms que le servo atteigne sa angle
  }
}
