Voir la [version excel](materielGrove.xlsx)

(cliquer sur l'image pour voir un exemple de mise en oeuvre)

| <div style="width:70px">Composant </div> | Fonction          | Type | signal | [bibliothèque](bibliotheque/capteur/)  |
|:---------|:---------------------|:---------------|:---------------|:---------------|
| <div style="width:70px">[![](grove/Loudness_Sensor_new.jpg)](https://wiki.seeedstudio.com/Grove-Loudness_Sensor/) </div> | Capteur de niveau sonore  | capteur | [analogique](capteurs/analogique/)|
|<div style="width:70px"> [![](grove/UV_Sensor_01.jpg)](https://wiki.seeedstudio.com/Grove-UV_Sensor/)</div>						| Capteur d'Ultraviolets    | capteur | [analogique](capteurs/analogique/)|
|<div style="width:70px"> [![](grove/Grove_Temperature_Sensor_View.jpg)](https://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/)</div>						| Thermometre    | capteur | [analogique](capteurs/analogique/)  | |
| <div style="width:70px">[![](grove/humidity.jpg)](https://wiki.seeedstudio.com/Grove-TemperatureAndHumidity_Sensor/)	</div>					| Capteur d'humidité et de température  | capteur | analogique  |  Seeed DHT library |
| <div style="width:70px">[![](bibliotheque/capteur/capteurUltrason.jpeg)](https://wiki.seeedstudio.com/Grove-Ultrasonic_Ranger/)</div>						| Télémètre à ultrasons    | capteur | analogique  |ultrasonic ranger |
|<div style="width:70px"> [![](grove/Grove_Air_Quality_Sensor_big.jpg)](https://wiki.seeedstudio.com/Grove-Air_Quality_Sensor_v1.3/)</div>						|Capteur de qualité de l'air  | capteur |   |  AirQuality_Sensor   |
| <div style="width:70px">[![](grove/Tilt1.jpg)](https://wiki.seeedstudio.com/Grove-Tilt_Switch/)	</div>					| Détecteur de bascule      | capteur | [numerique](capteurs/numerique/)  | |
|<div style="width:70px"> [![](grove/surface.jpg)](https://wiki.seeedstudio.com/Grove-Touch_Sensor/)	</div>					| Capteur de contact     | capteur | [numerique](capteurs/numerique/)  | |
|<div style="width:70px"> [![](grove/IR_reflectif.jpg)](https://wiki.seeedstudio.com/Grove-Infrared_Reflective_Sensor/)</div>						|Capteur de proximité d'obstacle    | capteur | [numerique](capteurs/numerique/)  | |
|<div style="width:70px"> [![](grove/Grove_-_PIR_Motion_Sensor.jpg)](https://wiki.seeedstudio.com/Grove-PIR_Motion_Sensor/)</div>| Capteur de mouvement     | capteur | [numerique](capteurs/numerique/)  | |
| <div style="width:70px">[![](grove/Grove_Speaker_01.jpg)](https://wiki.seeedstudio.com/Grove-Speaker/)	</div>					| Haut parleur amplifié     | actionneur | [analogique (tone)](https://www.arduino.cc/reference/en/language/functions/advanced-io/tone/)  | |
|<div style="width:70px"> [![](grove/Grove_Buzzer.jpg)](https://wiki.seeedstudio.com/Grove-Buzzer/)</div>						| Mini Buzzer     | actionneur | [analogique (tone)](https://www.arduino.cc/reference/en/language/functions/advanced-io/tone/)  | |
|<div style="width:70px"> [![](grove/Grove—Servo.jpg)](https://wiki.seeedstudio.com/Grove-Servo/)</div>						| Servomoteur    | actionneur | analogique  |Servo (pré-installée) |
|<div style="width:70px"> [![](grove/RGB_LCD.jpg)](https://wiki.seeedstudio.com/Grove-LCD_RGB_Backlight/)</div>						| Ecran LCD    | actionneur |   | Grove-LCD RGB Backlight |
|<div style="width:70px"> [![](grove/Grove-4_digit_display.jpg)](https://wiki.seeedstudio.com/Grove-4-Digit_Display/)</div>						|Afficheur 4 chiffres   | actionneur |   |  Grove-4-Digit Display  , TimerOne  |




