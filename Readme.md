

# Liste des documents

* exemple de stratégie de développement d'un projet : le [Teremin](Teremin/)
* exemple de projet avec un petit afficheur : [Compteur de Peuple](CompteurPeuple/)
* Liste des capteurs / actionneurs disponibles avec des indications sur leur mise en oeuvre: [listeMaterielGrove](listeMaterielGrove.md)


[[_TOC_]]




## Arduino Uno



Savoir trouver et installer l'IDE arduino, le mettre en oeuvre pour programmer une carte.

activer un actionneur

lire un capteur

installer et utiliser une librairie

### Broches

L'arduino est équipé d'entrées/sorties
![](Arduino-Uno-Pin-Diagram.png)

* analogiques (notées analog)
* entrées/ sorties numériques (notées digital)
* certaines sorties numériques peuvent produite une tension variable.Ce sont les sorties PWM

### Blink

premier exemple : faire clignoter une led
`Fichier > Exemples > Basics > Blink`

[schéma de câblage](Blink.fzz)


## Capteurs / actionneurs

### capteurs

Les capteurs permettent de lire une information venant de l'extérieur. Lorsqu'on met en œuvre un capteur, il peut être utile au cours du développement  de réaliser une liaison [série](#série) pour avoir un retour sur l'ordinateur, afin de bien comprendre comment celui-ci réagit

* [entree analogique](capteurs/analogique/)
* [entree numerique](capteurs/numerique/)

#### avec une bibliothèque 
Certains capteurs dialogue à l'aide d'un protocole spécifique, qui nécessite l'usage de code additionnel, qui simplifie leur usage.

exemple : [Capteur à ultrasons](bibiotheque/capteur/)

### actionneurs

#### sortie numérique 

L'arduino UNO est pourvu de 14 entrée/sorties numériques.

Une sortie numérique peut produire deux tensions : 0 ou 5V

On s'en sert pour activer / désactiver un actionneur simple (par exemple une led)

exemple : faire clignoter une led
`Fichier > Exemples > Basics > Blink`

[schéma de câblage](Blink.fzz)


#### sortie PWM 

Les sorties digitales marquées d'un ~  sont capables de produire une pseudo tension variable, comprise entre 0 et 5V.

`Fichier > Exemples > 01.Basics > Fade`


#### avec une bibliothèque 

certains  actionneurs dialoguent avec l'arduino à l'aide un protocole spécifique (I2C, série ...)

Dans ce cas une librairie est souvent disponible

* exemple : [Servomoteur](bibiotheque/actionneur/Servo)

D'autres composants disponibles peuvent avoir besoin d'une bibliothèque :

* [écran LCD RGB](https://wiki.seeedstudio.com/Grove-LCD_RGB_Backlight/) avec la bibiothèque ` Grove-LCD RGB Backlight`
* [Capteur de qualité de l'air](https://wiki.seeedstudio.com/Grove-Air_Quality_Sensor_v1.3/)
* [capteur de rythme cardiaque](https://wiki.seeedstudio.com/Grove-Finger-clip_Heart_Rate_Sensor/)
* [Leds RVB adressables neoPixel](https://www.adafruit.com/product/1643)

## Communication

### Série

La manière la plus simple d'établir une communication est d'utiliser la liaison série avec l'ordinateur.

Cette communication permet de transferer des messages entre l'arduino et l'ordinateur , et vice versa.

La liaison série peut être mise en oeuvre dans l'ordinateur, soit avec le moniteur série de L'IDE arduino, soit avec un programme réalisé avec processing, flash, puredata, max/msp, python etc... 

Tous les exemples suivants contiennent un code [processing](https://processing.org/) commenté en fin croquis, à copier coller dans processing pour éventuellement traiter les données dans ce programme.


#### arduino > ordinateur


L'exemple suivant communique à l'ordinateur la valeur d'une entrée analogique.

`Fichier > Exemples > 04.Communication > Graph`

Pour afficher la valeur reçue, après le téléchargement du croquis sur l'arduino, ouvrir :

`Outils > Moniteur série` affiche les valeurs numériques reçues

ou 

`Outils > Traceur série` affiche une courbe des valeurs numériques reçues
___

`Fichier > Exemples > 04.Communication > VirtualColorMixer` montre un exemple envoyant plusieurs valeurs à la fois.

Dans ce cas, seul le `Outils > Moniteur série` est utilisable.


#### ordinateur > arduino


##### analogique
`Fichier > Exemples > 04.Communication > Dimmer`

Cet exemple permet de régler la luminance d'une led à l'aide de valeurs comprises entre 0 et 255, envoyées via le moniteur série :

`Outils > Moniteur série`

##### numérique
`Fichier > Exemples > 04.Communication > PhysicalPixel`

Cet exemple permet d'activer ou de désactiver une sortie numérique en fonction d'un code envoyé depuis l'ordinateur ( en l'occurence, `H` ou `L`).




### ESP32 / wifi smartphone


L'ESP32 est une carte alternative à l'arduino UNO de base. Elle offre une connectivité wifi. Pour l'utiliser, il est nécessaire de charger la définition de cette carte.

piloter un ESP32 à l'aide d'un smartphone via wifi

exemple de programmation d'une carte tierce avec le logiciel arduino
Création d'un serveur web minimal et utilisation à l'aide d'un samrtphone

[à suivre...](https://www.aranacorp.com/fr/programmer-un-nodemcu-esp32-avec-lide-arduino/)

## Ressources

[FlossManual Arduino](https://fr.flossmanuals.net/arduino/historique-du-projet-arduino/)

[Forum Arduino Français](https://forum.arduino.cc/index.php?board=33.0)

[Fritzing](https://fritzing.org/) pour dessiner facilement un schéma

[Gallerie de projets Arduino](https://create.arduino.cc/projecthub)

[instructables](https://www.instructables.com/howto/arduino/)

### Liste de matériel

[materielGrove.xlsx](materielGrove.xlsx)



https://www.pololu.com/product/4079