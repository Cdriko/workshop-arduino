https://gitlab.com/Cdriko/workshop-arduino


# Introduction à arduino
Licence SYRDES - 15 Novembre 2023


## Objectifs

- Découvrir l'ecosystème arduino
- Apprendre les bases avec un Arduino Uno
- Mettre en oeuvre des capteurs et des actionneurs
- Établir une communication
- Pouvoir aller plus loin


## Programme

### Jour 1

#### Matin

- Tour de table et présentation

- Sondage rapide : Qui a déjà

	+ fait un cablage électronique (breadboard)
	+ écrit du code
	+ utilisé arduino

- Introduction à arduino
	+ principe d'un microcontrôleur
	+ capteur/actionneur
	+ petite histoire du projet arduino
	+ Spécificités de l'[Arduino UNO R4 Wifi](https://store.arduino.cc/products/uno-r4-wifi)
	+ installer le logiciel et faire clignoter une led
	
https://docs.arduino.cc/hardware/uno-r4-wifi
  + [plus de choses](R4/reame.md) avec le R4

#### Après-midi



- Communication : établir une communication série avec l'ordinateur
- Exercice : faire varier une led avec un capteur analogique (potentiometre)
- tour d'horizon des [capteurs et actionneurs disponibles](listeMaterielGrove.md)
- Mettre en œuvre un composant à l'aide d'une bibliothèque : [Capteur de température et humidité DHT-11](https://randomnerdtutorials.com/esp32-dht11-dht22-temperature-humidity-sensor-arduino-ide/)




### Jour 2


- Mettre en œuvre un mini serveur web sur l'ESP32
	+ [exemple 1](https://www.aranacorp.com/fr/creez-une-interface-web-pour-piloter-votre-nodemcu-esp32/)
	+ [exemple 2](https://randomnerdtutorials.com/esp8266-nodemcu-async-web-server-espasyncwebserver-library/)
	+ [communication entre deux ESPs](https://www.aranacorp.com/fr/communication-entre-deux-esp8266-par-wifi/)
	+ [créer un point d'accès](https://tommydesrochers.com/tout-savoir-sur-le-wifi-avec-lesp32-avec-et-sans-wifimanager-ep2/)

	

- DMX : essai d'un interfaçage avec un dispositif DMX
