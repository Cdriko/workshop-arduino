/*
affiche la distance mesuree par un lidar sur un ruban de leds
*/


// Change this to match the Arduino pin connected to the sensor's OUT pin.
const uint8_t sensorPin = 4;


//pour les leds
#include <Adafruit_NeoPixel.h>  //charger la librairie
Adafruit_NeoPixel pixels(10, 6, NEO_GRB + NEO_KHZ800);


void setup() {
  //allumer une liaison série
  Serial.begin(115200);
  //initialise le ruban de led
  pixels.begin();  // INITIALIZE NeoPixel strip
}




void loop() {

  //mesurer la distance avec le lidar et la stocker dans une variable
  int distance = mesureDistance();
  Serial.print("Distance :");
  Serial.println(distance);
  //calculer le nombre de leds allumées
  int nombreDeLedsAllumees = int(map(distance, 0, 3000, 0, 10));

Serial.print("nombreDeLedsAllumees :");
  Serial.println(nombreDeLedsAllumees);
  //customiser la couleur
  // gerer l'affichage
  pixels.clear();
  for (int i = 0; i < 10; i++) {
    if (i < nombreDeLedsAllumees) {
      pixels.setPixelColor(i, pixels.Color(100, 150,100));
    }
  }
  pixels.show(); 
}


int mesureDistance() {
  int16_t t = pulseIn(sensorPin, HIGH);

  if (t == 0) {
    // pulseIn() did not detect the start of a pulse within 1 second.
    Serial.println("timeout");
    return(3000);
  } else if (t > 1850) {
    // No detection.

    return (-1);
  } else {
    // Valid pulse width reading. Convert pulse width in microseconds to distance in millimeters.
    int16_t d = (t - 1000) * 4;

    // Limit minimum distance to 0.
    if (d < 0) { d = 0; }

    return (d);
  }
}