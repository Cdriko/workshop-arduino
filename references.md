# réferences artistiques et créatives


## Quelques œuvres

[Chorale à roulettes](http://alexandre.quessy.net/?q=rotarianchoir) d'Alexandre Quessy


[Solenoides](https://wiki.labomedia.org/index.php/Soleno%C3%AFdes_Midi.html#Videos) par gratuit music

[autoChoregraphie](https://www.creativeapplications.net/processing/self-choreographing-network-cyber-physical-design-and-interactive-bending-active-systems/) par V.Soana et M.Maierhofer

[Robots tisserants](https://kayserworks.com/fiberbots/) par M.Kayser

[onversation au fil de l'eau](https://babiole.net/conversation-fil-de-leau/) par cécile Babiole